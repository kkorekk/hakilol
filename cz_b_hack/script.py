import itertools
import random
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
import requests

SLEEP_FROM = 60
SLEEP_TO = 120
SPAMMER_NAME = "mornel"

EMAILS_LOG = "emails.log"

# URLS
FORM_URL = "https://forms.gle/aUPkEJRuFwq7biSP9"
EMAILS_URL_TEMPLATE = "https://polimorfizm.pl/pokaz_maile.php?kto={spammer}&strona={page}"

# ELEMENTS
EMAIL_XPATH = '//input[@type="email" and @aria-label="Twój adres e-mail"]'
RADIO_BUTTON_CLASS_NAME = "docssharedWizToggleLabeledContainer"
BAND_NAME = "Polimorfizm"
SUBMIT_XPATH = '//*[text()="Prześlij"]'

options = webdriver.ChromeOptions()
options.add_argument("-incognito")


def random_sleep(from_=0.5, to=2.0):
    r = round(random.uniform(from_, to), 2)
    print(f"sleeping for {r}")
    time.sleep(r)

def fill_form(email):
    print(f"voting as {email}")
    browser = webdriver.Chrome(options=options)
    browser.get(FORM_URL)

    email_el = browser.find_element(By.XPATH, EMAIL_XPATH)
    email_el.send_keys(email)

    for el in browser.find_elements(By.CLASS_NAME, RADIO_BUTTON_CLASS_NAME):
        if el.text == BAND_NAME:
            el.click()

    send = browser.find_element(By.XPATH, SUBMIT_XPATH)
    send.click()

    browser.close()

def fetch_emails():
    for page in itertools.count(0):
        print(f"fetching page: {page}")
        res = requests.get(EMAILS_URL_TEMPLATE.format(spammer=SPAMMER_NAME, page=page))

        emails = res.content.decode("utf-8").split("<br>")
        emails.remove("")

        if not emails:
            return

        for e in emails:
            yield e

if __name__ == "__main__":
    with open(EMAILS_LOG, "a+") as f_log:
        f_log.seek(0)
        emails_seen = set(l for l in f_log.read().splitlines())
        counter = len(emails_seen)

        for email in fetch_emails():
            if email in emails_seen:
                continue
            
            fill_form(email)
            emails_seen.add(email)
            f_log.write(f"{email}\n")
            counter += 1
            print(f"send email number {counter}")
            random_sleep(SLEEP_FROM, SLEEP_TO)
