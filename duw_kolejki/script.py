import datetime
import getpass
import os
import random
import time

from playsound import playsound
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.common.by import By

### URL SETTINGS
DUW_RESERVATION_PAGE = "https://rezerwacje.duw.pl/pol/queues/99/18/{date}"
DATE_FORMAT = "%Y-%m-%d"

### ELEMENTS
EMAIL_XPATH = '//input[@id="UserEmail"]'
PASSWORD_XPATH = '//input[@id="UserPassword"]'
SUBMIT_XPATH = '//input[@type="submit"]'
RESERVATION_NOT_AVAILABLE = '//h4[normalize-space(text()) = "Brak wolnych terminów."]'

### BROWSER SETTINGS
options = webdriver.ChromeOptions()
options.add_argument("-incognito")


def get_browser(options):
    return webdriver.Chrome(options=options)

def random_sleep(lbound=0.5, ubound=2.5):
    time.sleep(round(random.uniform(lbound, ubound), 2))

def find_free_slot(browser, username, password):
    date = datetime.datetime.now() + datetime.timedelta(days=1)
    is_date_available = True

    while is_date_available:
        date_str = date.strftime(DATE_FORMAT)
        url = DUW_RESERVATION_PAGE.format(date=date_str)
        print(f"testing {url}")
        browser.get(url)

        if "error404" in browser.current_url:
            is_date_available = False
            continue

        if "login" in browser.current_url:
            login(browser, username, password)
            if date_str not in browser.current_url:
                continue
        
        check_for_alert(browser)

        date += datetime.timedelta(days=1)
        random_sleep()

def check_for_alert(browser):
    try:
        el = browser.find_element(By.XPATH, RESERVATION_NOT_AVAILABLE)
        if el.is_displayed():
            return
    except exceptions.NoSuchElementException:
        pass

    playsound("siren.mp3")
    input("Use browser in order to book free slot, waiting for <ENTER>")

def login(browser, username, password):
    email_el = browser.find_element(By.XPATH, EMAIL_XPATH)
    email_el.send_keys(username)

    password_el = browser.find_element(By.XPATH, PASSWORD_XPATH)
    password_el.send_keys(password)

    submit_el = browser.find_element(By.XPATH, SUBMIT_XPATH)
    submit_el.click()

def get_credentials():
    username = os.getenv("DUW_USERNAME")
    if not username:
        username = input("Username: ")

    password = os.getenv("DUW_PASSWORD")
    if not password:
        password = getpass.getpass()

    return username, password

if __name__ == "__main__":
    username, password = get_credentials()
    browser = get_browser(options)
    while True:
        print("looking for a free slot")
        find_free_slot(browser, username, password)

        print("free slot not found - sleeping")
        random_sleep(60.0, 180.0)
